#include "ofApp.h"
//--------------------------------------------------------------

void ofApp::setup(){
    // make the app window large enough to fit all three images
    ofSetWindowShape(1024,1024);
    
    //uncomment the following line if you want a verbose log (which means a lot of info will be printed)
    //ofSetLogLevel(OF_LOG_VERBOSE);
    
    // setup Runway
    runway.setup(this, "http://localhost:8000");
    runway.start();
    
    //fill up our vector with an initial set of random values
    for (int i = 0; i < 512; i++) { //check to make sure this is the right vector size
        float n = ofRandom(-3.0, 3.0); //look at a sample vector to see what range you should use
        v.push_back(n);
    }
    
    float Speed = 1.0;
}
//--------------------------------------------------------------
void ofApp::update(){
    
    if(!runway.isBusy()){ //check if runway is still processing your last request
        
        ofxRunwayData data;
        int frame = ofGetFrameNum(); //use time to adjust the noise
        
        for (int i = 0; i < 512; i++) {
            v[i] += ofSignedNoise((i+1)*frame/100.0)/Speed; //adjust all the floats using noise
        }
        
        data.setFloats("z", v); //load the vectors into the data
        
        // query Runway
        runway.send(data); //send the data to runway
    }
    
    runway.get("image", runwayResult); //get the latest image from runway
}
//--------------------------------------------------------------
void ofApp::draw(){
    
    
    // draw image received from Runway
    if (runwayResult.isAllocated()) {
        runwayResult.draw(0, 0); //if we have an image, draw it.
    }
}
//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    if(key == ' '){ //press spacebar to generate a new vector
        v.clear(); //clear the vector
        for (int i = 0; i < 512; i++) {
            float n = ofRandom(-3.0, 3.0); //load the vector with new floats
            v.push_back(n);
        }
    }
    if(key == 's'){ //press w to increase up
        Speed = Speed * 2;
    }
    if(key == 'w'){ //press s to decrease up
        Speed = Speed/2;
    }
}


// Runway sends information about the current model
//--------------------------------------------------------------
void ofApp::runwayInfoEvent(ofJson& info){
    ofLogNotice("ofApp::runwayInfoEvent") << info.dump(2);
}
// if anything goes wrong
//--------------------------------------------------------------
void ofApp::runwayErrorEvent(string& message){
    ofLogNotice("ofApp::runwayErrorEvent") << message;
}
//--------------------------------------------------------------

