Title: Grateful Dead Artwork Shuffler

Video: https://youtu.be/YuVOnSW304Y

Description: 

In making the Grateful Dead artwork model I really liked looking through the different results and 
was intrigued by the different types that came out. Some were had circular shapes which is the iconic Grateful
Dead logo, others were a consistent pattern accross the screen and others resembled the bands' album covers.
For this reason, I wanted to make a project that allowed the viewer to shuffle through the results and speed the
metamorphosis or slow it down depending.

I included a function that uses keys to increase and decrease the speed at which the program shows
the iterations. The w key increases speed while the s key decreases speed. In the video, this can be seen as
iterations increase in speed, slow down, and then increase again.